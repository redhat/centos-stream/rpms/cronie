#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/cronie/Regression/echos-OK-twice-in-init-script
#   Description: Test for echos "OK" twice in init script
#   Author: Martin Cermak <mcermak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="cronie"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TEMPDIR=\`mktemp -d\`" 0 "Creating tmp directory"
        rlRun "pushd $TEMPDIR"

        rlFileBackup "/etc/rc.d/init.d/crond"
        rlServiceStop crond

cat > /etc/rc.d/init.d/functions2 <<EOF1
success() {
        echo OK >> $TEMPDIR/log.txt
}
EOF1
        rlRun "cat /etc/rc.d/init.d/functions2"

        sed -i "s/^\..*$/\0\n\. \/etc\/rc\.d\/init\.d\/functions2/" /etc/rc.d/init.d/crond
        rlRun "grep ^\\\. /etc/rc.d/init.d/crond"
    rlPhaseEnd

    rlPhaseStartTest
        rlAssertNotExists log.txt
        rlServiceStart crond
        rlAssertExists log.txt

        rlRun "TIMES_PRINTED_OK=`grep OK log.txt | wc -l`"
        rlRun "test $TIMES_PRINTED_OK -eq 1"

        rlServiceStop crond
    rlPhaseEnd

    rlPhaseStartCleanup
        rlBundleLogs TESTLOGS /etc/rc.d/init.d/functions2 /etc/rc.d/init.d/crond
        rlRun "rm -f /etc/rc.d/init.d/functions2"
        rlFileRestore
        rlServiceRestore crond
        rlRun "popd"
        rlRun "rm -r $TEMPDIR" 0 "Removing tmp directory"
        #avoid systemd failing to start crond due start-limit
        sleep 10
    rlPhaseEnd
rlJournalEnd
