#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/cronie/Regression/cronie-jobs-environment
#   Description: testing EUID with jobs are executed and if LANG is correctly set
#   Author: Jakub Prokes <jprokes@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1


## Is nice to know when things going wrong
trap 'errorHandler ${LINENO}' ERR;
function errorHandler() {
    local code="${?}";
    local lineNO="$1";

    case $code in
        127)
            rlFail "Command not found on line $lineNO"
            ;;
        *)
            rlFail "Unhandled error $code near line $lineNO";
            ;;
    esac
}

function makeUser() {
    local userName="$(tr -dc "[:lower:]" < /dev/urandom | head -c 5 | sed 's/^/qa_/')";
    while getent passwd $userName; do
        userName="$(tr -dc "[:lower:]" < /dev/urandom | head -c 5 | sed 's/^/qa_/')";
    done
    useradd -d $tmpDir $userName;
    echo $userName;
}


PACKAGE="cronie"
declare -r LIBFAKETTIMEPACKAGE="libfaketime-0.9.1.tar.gz";
declare -r LIBFAKETIME="libfaketime.so.1";

rlJournalStart
    rlPhaseStartSetup "Setup test environment"
        rlAssertRpm $PACKAGE
        rlRun "tmpDir=\$(mktemp -d)" 0 "Creating tmp directory";
        testDir="$(pwd)";
	rlRun "chmod 755 $tmpDir" 0 "Setting permissions for tmp directory";
	ls -lahdZ $tmpDir;
        rlRun "pushd $tmpDir"
        testUser="$(makeUser)";
        rlRun "getent passwd $testUser" 0 "Test user $testUser created";
        rlServiceStop crond;
    rlPhaseEnd

    rlPhaseStartSetup "Prepare libfaketime";
        rlRun "cp ${testDir}/$LIBFAKETTIMEPACKAGE ./" 0 "Get library sources";
        rlRun "tar -xvzf $LIBFAKETTIMEPACKAGE" 0 "Unpack library sources";
        pushd ${LIBFAKETTIMEPACKAGE%.tar.gz};
        rlRun "make &>/dev/null" 0 "Building library from sources";
        rlRun "cp ./src/$LIBFAKETIME ../" 0 "Coping library to working directory";
        popd;
        LD_PRELOAD=./libfaketime.so.1 FAKETIME='1994-07-29 12:00:01' date +%G | { year="$(cat)"; [[ $year -eq 1994 ]]; };
        rlAssert0 "Library preloading working well" $?;
    rlPhaseEnd

    rlPhaseStartSetup "Prepare tests";
        rlRun "sed 's#%TMPDIR#$tmpDir#' $testDir/crontab.temp > crontab.source" #0 "Crontab prepared from template";
        rlRun "crontab -u $testUser crontab.source" 0 "Set crontab for test user";
        crontab -lu $testUser;
        rlRun "cp $testDir/cron_test.sh ./" 0 "Copyed script for cron job";
        rlRun "chmod a+x cron_test.sh" 0 "Permissions to executed set";
	rlRun "[[ -e /var/spool/mail/$testUser ]] && printf '' > /var/spool/mail/$testUser" 0 "Clean up mails"
        rlLog "Execute crond with faked time";
        LD_PRELOAD=./libfaketime.so.1 FAKETIME='@1994-07-29 12:12:50' /usr/sbin/crond -n -x sch &
        cronPID=$!;
	pstree -Aph
        rlRun "kill -0 $cronPID" 0 "crond is running";
        rlLog "Security timeout for 30 sec to ensure all configs are loaded and crontab is succesfully processed";
        sleep 30;
    rlPhaseEnd

    rlPhaseStartTest "cronie drops \$LANG and never passes it on to jobs run"
        rlRun "[[ $(sed -n '/^LANG/s/LANG=//p' /tmp/cronie-jobs-environment.log) = $LANG ]]" 0 "LANG is set"
    rlPhaseEnd

    rlPhaseStartTest "cronie doesn't drop privileges before popen"
	    rlRun "[[ $(sed -n '/^EUID/s/EUID=//p' /tmp/cronie-jobs-environment.log) -eq $(id -u $testUser) ]]" 0 \
            "Crontab is executed with correct EUID";
    rlPhaseEnd

    rlPhaseStartCleanup
	rlRun "kill $cronPID" 0 "Terminating crond"
        rlRun "crontab -ru $testUser" 0 "Crontab removed"
        rlRun "popd"
        rlRun "rm -r $tmpDir" 0 "Removing tmp directory"
        rlRun "userdel -rf $testUser";
        rlServiceRestore crond;
        #avoid systemd failing to start crond due start-limit
        sleep 10
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
